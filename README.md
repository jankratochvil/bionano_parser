Shell scripts for running cmake and make: 'cmake.sh', 'make.sh'. Run these from root directory.

Make generates the following:

* Executable 'build/bionano_parser' that reads sample BED, XMAP and SMAP files and prints a few records to stdout.
* Library 'build/libbionano_api.so' that exposes C functions for reading a BED file.
* Executable 'build/bionano_test' for testing 'libbionano_api.so'.

Shell script to clean ALL build files: 'clean.sh'


