#!/bin/bash
echo ""
mkdir -p build
# some undocumented cmake parameters
# -H: where to look for CMakeLists.txt
# -B: build directory
# genereate build files
# echo "$(cmake -H. -Bbuild)"
# build the target
# echo "$(cmake --build build --clean-first)"

cd build
echo "$(cmake ..)"
echo ""
wait
cp -n "compile_commands.json" --target-directory=..


