// file:	cTokenizer.h
// version: 1.0
// summary:	A Simple Tokenizer
// author:  Petr Gajdo

#pragma once
#ifndef __CTOKENIZER_H_
#define __CTOKENIZER_H_

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <unordered_set>
#include "rgb.h"

namespace pg
{
	enum class TokenizableType
	{
		TT_FLOAT,
		TT_DOUBLE,
		TT_INT,
		TT_UINT,
		TT_CHAR,
		TT_STRING,
		TT_RGB,
		TT_UNDEFINED
	};

	class IHeader
	{
	public:
		// virtual bool parseHeaderInfo(void) = 0;
		virtual bool parseLine(std::string &s) = 0;
	};

	class BaseHeader : public IHeader
	{
	public:
		BaseHeader()
		{
		}
		// read lines on header input
		// return true when header accepted the string
		// return false when header is finished reading
		inline bool parseLine(std::string &s) override
		{
			return false;
		}
	};

	template<typename T=BaseHeader>
	class ITokenizable
	{
	public:
		T m_header;
		std::vector<char> m_separators;

	protected:
		ITokenizable() {}

	public:
		virtual ~ITokenizable() {}
		virtual TokenizableType getItemType(const unsigned int id) = 0;
		virtual unsigned int getNumberOfItems() = 0;
		virtual unsigned int getNumberOfRequiredItems() = 0;
		virtual void setValue(const unsigned int id, const std::string::iterator& start, const std::string::iterator& end) = 0;

	protected:

		inline void toF(const std::string::const_iterator& start, const std::string::iterator& end, float &value) const
		{
			char tmp = *end;
			*end = '\0';
			value = static_cast<float>(atof(&*start));
			*end = tmp;
		}

		inline void toD(const std::string::const_iterator& start, const std::string::iterator& end, double &value) const
		{
			char tmp = *end;
			*end = '\0';
			value = atof(&*start);
			*end = tmp;
		}

		inline void toI(const std::string::const_iterator& start, const std::string::iterator& end, int &value) const
		{
			char tmp = *end;
			*end = '\0';
			value = atoi(&*start);
			*end = tmp;
		}

		inline void toUI(const std::string::const_iterator& start, const std::string::iterator& end, unsigned int &value) const
		{
			char tmp = *end;
			*end = '\0';
			value = static_cast<unsigned int>(atoi(&*start));
			*end = tmp;
		}

		inline void toUI(const std::string::const_iterator& start, const std::string::iterator& end, unsigned int &value, const unsigned int &id) const
		{
			toUI(start, end, value);

			if (value == static_cast<unsigned int>(-1))
			{
				std::cout << "unsigned int as '-1' value for field id=" << id << std::endl;
				getchar();
			}
		}

		inline void toC(const std::string::const_iterator& start, const std::string::iterator& end, char &value) const
		{
			value = *start;
		}

		inline void toS(const std::string::const_iterator& start, const std::string::iterator& end, std::string &value) const
		{
			value = std::string(start, static_cast<std::string::const_iterator>(end));
		}
		inline void toRGB(const std::string::const_iterator& start, const std::string::iterator& end, Rgb &value) const
		{
			std::string tmp = std::string(start, static_cast<std::string::const_iterator>(end));
			std::string::size_type prevPos = 0, pos = 0;

			pos = tmp.find(',', pos);
			if (pos == std::string::npos) {
				std::cout << "Error reading rgb value '" << tmp << "'" << std::endl;
				exit(1);
			}
			value.r = atoi(tmp.substr(prevPos, pos - prevPos).c_str());
			prevPos = ++pos;

			pos = tmp.find(',', pos);
			if (pos == std::string::npos) {
				std::cout << "Error reading rgb value '" << tmp << "'" << std::endl;
				exit(1);
			}
			value.g = atoi(tmp.substr(prevPos, pos - prevPos).c_str());
			prevPos = ++pos;

			pos = tmp.find(',', pos);
			value.b = atoi(tmp.substr(prevPos, pos - prevPos).c_str());
		}
	};

	// template<typename T, typename U>
	template<typename T>
	class CTokenizer
	{
		/* static_assert(std::is_base_of<ITokenizable<U>, T>::value, "T must inherit from ITokenizable"); */

	public:
		using value_type = T;

		std::unordered_set<char> m_separator;
		std::unordered_set<char>::const_iterator m_separatorIter;

	public:
		CTokenizer() {}
		~CTokenizer(void) {}

	protected:
		bool parseLine(std::string& s, T& record);
		bool parseHeader(std::string& s, T& record);

	public:
		inline bool addSeparator(const char c)
		{
			m_separator.insert(c);
			m_separatorIter = m_separator.find(c);
			return (m_separatorIter != m_separator.end());
		}

		unsigned int tokenize(const char* fileName, std::vector<T> *records);
	};

	// template<typename T, typename U>bool CTokenizer<T,U>::parseLine(std::string& s, T& record)
	template<typename T>bool CTokenizer<T>::parseLine(std::string& s, T& record)
	{
		auto end = s.end();
		auto start = end;

		unsigned int recordItemID = 0;

		for (auto it = s.begin(); it != end; ++it)
		{
			m_separatorIter = m_separator.find(*it);
			if (m_separatorIter == m_separator.end())   //if (*it != delimiter)
			{
				if (start == end)
					start = it;
				continue;
			}
			if (start != end) {
				record.setValue(recordItemID, start, it);
				++recordItemID;
				start = end;
			}
		}
		if (start != end)
		{
			record.setValue(recordItemID, start, end);
			++recordItemID;
		}
		// require at least numRequiredItems and allow max numItems
		return recordItemID >= record.getNumberOfRequiredItems()
			&& recordItemID <= record.getNumberOfItems();
	}

	// template<typename T, typename U>unsigned int CTokenizer<T,U>::tokenize(const char* fileName, std::vector<T> *records)
	template<typename T>unsigned int CTokenizer<T>::tokenize(const char* fileName, std::vector<T> *records)
	{
		// std::cout << fileName << std::endl;
		unsigned int noRecords = 0;
		std::ifstream in;
		std::string s;

		in.open(fileName, std::ios::in);
		if (!in.is_open())
		{
			std::cout << "Error ... (tokenize)" << std::endl;
			return 0;
		}

		T tmpRecord;
		while(std::getline(in, s))
		{
			// feed the lines into header until it is finished reading
			if (tmpRecord.m_header.parseLine(s) == false) 
			{
				// finished
				break;
			}
		}
		// now file content
		do
		{
			if (!parseLine(s, tmpRecord))
			{
				std::cout << "Bad record on line " << noRecords+1 << " in the input file ... " << std::endl;
			}
			else
			{
				records->emplace_back(tmpRecord);
				noRecords++;
			}
		}
		while (std::getline(in, s));

		in.close();
		return noRecords;
	}

}

#endif // __CTOKENIZER_H_
