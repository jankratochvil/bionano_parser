#include 	"BED.h"
#include 	"XMAP.h"
#include 	"SMAP.h"


template<typename T>
class TokenizerWrapper
{
private:
	pg::CTokenizer<T> m_tokenizer;
	std::vector<T> m_records;
	typename std::vector<T>::iterator m_recordsIterator;

public:
	T m_currentRecord;

public:
	TokenizerWrapper(void)
	{
	}

	bool readFile(const char* filename)
	{
		m_records.clear();
		pg::CTokenizer<T> tokenizer;
		tokenizer.addSeparator('\t');
		int num = tokenizer.tokenize(filename, &m_records);
		if (num == 0)
		{
			/* std::cout << "Error reading file '" << filename << std::endl; */
			return false;
		}
		m_recordsIterator = m_records.begin();
		return true;
	}

	unsigned int getNumOfRecords(void) const
	{
		return m_records.size();
	}

	bool getNextRecord(void)
	{
		if (m_recordsIterator == m_records.end())
		{
			/* std::cout << "iterator = end" << std::endl; */
			return false;
		}
		else
		{
			m_currentRecord = *m_recordsIterator;
			++m_recordsIterator;
			/* std::cout << "advancing iterator" << std::endl; */
			return true;
		}
	}

	bool hasRecord(void) const
	{
		return m_recordsIterator != m_records.end();
	}
};

typedef TokenizerWrapper<bp::RecordBED>* BEDTokenizer;

extern "C"
{
	BEDTokenizer TokenizerWrapper_Create()
	{
		return new TokenizerWrapper<bp::RecordBED>();
	}

	void TokenizerWrapper_Delete(BEDTokenizer tokenizer)
	{
		if (tokenizer != NULL)
		{
			delete tokenizer;
			tokenizer = NULL;
		}
	}

	bool TokenizerWrapper_ReadFile(BEDTokenizer	tokenizer, const char* filename)
	{
		return tokenizer->readFile(filename);
	}

	bool TokenizerWrapper_HasRecord(BEDTokenizer tokenizer)
	{
		return tokenizer->hasRecord();
	}

	unsigned int TokenizerWrapper_GetNumOfRecords(BEDTokenizer tokenizer)
	{
		return tokenizer->getNumOfRecords();
	}

	bool TokenizerWrapper_GetNextRecord(BEDTokenizer tokenizer)
	{
		return tokenizer->getNextRecord();
	}

	const char* TokenizerWrapper_GetChrom(BEDTokenizer tokenizer)
	// std::string TokenizerWrapper_GetChrom(BEDTokenizer tokenizer)
	/* void TokenizerWrapper_GetChrom(BEDTokenizer tokenizer, char** chrom) */
	{
		  /* *ErrorMsg = (char*)::CoTaskMemAlloc(strlen(szErrorMessage) + 1); */
		// *chrom = new char(tokenizer->m_currentRecord.m_chrom.length());
		// strcpy(
		// *chrom = tokenizer->m_currentRecord.m_chrom.c_str();
		return tokenizer->m_currentRecord.m_chrom.c_str();
		/* return tokenizer->m_currentRecord.m_chrom; */
	}

	unsigned int TokenizerWrapper_GetChromStart(BEDTokenizer tokenizer)
	{
		return tokenizer->m_currentRecord.m_chromStart;
	}

	unsigned int TokenizerWrapper_GetChromEnd(BEDTokenizer tokenizer)
	{
		return tokenizer->m_currentRecord.m_chromEnd;
	}

}

/* extern "C" */
/* bool readFileBED(const char* filename, std::vector<bp::RecordBED> &records); */
/*  */
/* extern "C" */
/* bool readFileXMAP(const char* filename, std::vector<bp::RecordXMAP> &records); */
/*  */
/* extern "C" */
/* bool readFileSMAP(const char* filename, std::vector<bp::RecordSMAP> &records); */





