#include 	<stdio.h>
#include 	<sstream>

#include 	"path.h"
#include 	"BED.h"
#include 	"XMAP.h"
#include 	"SMAP.h"

using namespace pg;
using namespace bp;

static void testBED(const char* filename)
{
	std::cout << "Reading file '" << filename << "'" << std::endl;

	CTokenizer<RecordBED> tokenizer;
	//Separators
	tokenizer.addSeparator('\t');

	std::vector<RecordBED> records;
	tokenizer.tokenize(filename, &records);

	int r = records.size();
	int n = r < 10 ? r : 10;
	std::cout << "No records: " << r << ", printing first " <<  n << " records " << std::endl;
	for (auto &x : records)
	{
		x.print();
		if (--n == 0) break;
	}
	std::cout << std::endl;
}

static void testXMAP(const char* filename)
{
	std::cout << "Reading file '" << filename << "'" << std::endl;

	CTokenizer<RecordXMAP> tokenizer;
	//Separators
	tokenizer.addSeparator('\t');

	std::vector<RecordXMAP> records;
	tokenizer.tokenize(filename, &records);

	int r = records.size();
	int n = r < 10 ? r : 10;
	std::cout << "No records: " << r << ", printing first " <<  n << " records " << std::endl;
	for (auto &x : records)
	{
		x.print();
		if (--n == 0) break;
	}
	std::cout << std::endl;
}

static void testSMAP(const char* filename)
{
	std::cout << "Reading file '" << filename << "'" << std::endl;

	CTokenizer<RecordSMAP> tokenizer;
	//Separators
	tokenizer.addSeparator('\t');

	std::vector<RecordSMAP> records;
	tokenizer.tokenize(filename, &records);

	int r = records.size();
	int n = r < 10 ? r : 10;
	std::cout << "No records: " << r << ", printing first " <<  n << " records " << std::endl;
	for (auto &x : records)
	{
		x.print();
		if (--n == 0) break;
	}
	std::cout << std::endl;
	std::cout << std::endl;
}


int main(int argc, const char* argv[])
{
	std::stringstream ss;

	/* ss << bionanoparser_TESTFILES_PATH << "exp_refineFinal1_merged_filter_inversions_indel_ucscFormat.bed"; */
	ss << bionanoparser_TESTFILES_PATH << "test.bed";
	testBED(ss.str().c_str());

	// clear string stream
	ss.str(std::string());
	/* ss << bionanoparser_TESTFILES_PATH << "exp_refineFinal1_merged.xmap"; */
	ss << bionanoparser_TESTFILES_PATH << "output.xmap";
	testXMAP(ss.str().c_str());

	ss.str(std::string());
	ss << bionanoparser_TESTFILES_PATH << "variants_combine_filters_inMoleRefine1.smap";
	testSMAP(ss.str().c_str());
    return 0;
}

