#include "tokenizers.h"
#include "path.h"
#include <sstream>
#include <iostream>

int main(int argc, char *argv[])
{
	BEDTokenizer tokenizer = TokenizerWrapper_Create();

	std::stringstream ss;
	ss << bionanoparser_TESTFILES_PATH << "test.bed";
	
	std::string path = ss.str();
	std::cout << "Reading file '" << path.c_str() << "'" << std::endl;

	std::cout << TokenizerWrapper_ReadFile(tokenizer, path.c_str()) << std::endl;
	bool has = TokenizerWrapper_HasRecord(tokenizer);
	std::cout << has << std::endl;
	std::cout << TokenizerWrapper_GetNumOfRecords(tokenizer) << std::endl;

	while (TokenizerWrapper_GetNextRecord(tokenizer))
	{
		std::cout << TokenizerWrapper_GetChrom(tokenizer) << "\t"
			<< TokenizerWrapper_GetChromStart(tokenizer) << "\t"
			<< TokenizerWrapper_GetChromEnd(tokenizer) << "\t" 
			<< std::endl;
	}

	TokenizerWrapper_Delete(tokenizer);
	
	return 0;
}
