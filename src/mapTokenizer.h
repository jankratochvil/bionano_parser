// file:	cTokenizer.h
// version: 1.0
// summary:	A Simple Tokenizer
// author:  Petr Gajdo

#pragma once
#ifndef __MAPTOKENIZER_H_
#define __MAPTOKENIZER_H_

#include "cTokenizer.h"

namespace pg
{
	template<typename T>
	class MapTokenizer : public CTokenizer<T>
	{

	public:
		MapTokenizer() : CTokenizer<T>() {}
		~MapTokenizer(void) {}

	protected:
		bool parseLine(std::string& s, T& record);

	using CTokenizer<T>::parseLine;

	template<typename T>bool MapTokenizer<T>::parseLine(std::string& s, T& record)
	{
		std::cout << "test" << std::endl;
		return;
		auto end = s.end();
		auto start = end;

		unsigned int recordItemID = 0;

		for (auto it = s.begin(); it != end; ++it)
		{
			m_separatorIter = m_separator.find(*it);
			if (m_separatorIter == m_separator.end())   //if (*it != delimiter)
			{
				if (start == end)
					start = it;
				continue;
			}
			if (start != end) {
				record.setValue(recordItemID, start, it);
				++recordItemID;
				start = end;
			}
		}
		if (start != end)
		{
			record.setValue(recordItemID, start, end);
			++recordItemID;
		}
		return recordItemID >= record.getNumberOfRequiredItems()
			&& recordItemID <= record.getNumberOfItems();
	}

	template<typename T>unsigned int MapTokenizer<T>::tokenize(const char* fileName, std::vector<T> *records)
	{
		unsigned int noRecords = 0;
		std::ifstream in;
		std::string s;

		in.open(fileName, std::ios::in);
		if (!in.is_open())
		{
			std::cout << "Error ... (tokenize)" << std::endl;
			return 0;
		}

		T tmpRecord;
		while (std::getline(in, s))
		{
			if (!parseLine(s, tmpRecord))
			{
				std::cout << "Bad record on line " << noRecords+1 << " in the input file ... " << std::endl;
			}
			else
			{
				records->emplace_back(tmpRecord);
				noRecords++;
			}
		}
		in.close();
		return noRecords;
	}

}

#endif // __MAPTOKENIZER_H_
