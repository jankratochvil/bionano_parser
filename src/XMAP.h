#pragma once
#ifndef __XMAP_H_
#define __XMAP_H_

#include "cTokenizer.h"

// bionanoparser
namespace bp
{

class HeaderXMAP : public pg::IHeader
{
protected:
	std::vector<std::string> m_lines;

public:
	inline bool parseLine(std::string &s) override
	{
		if (s.at(0) == '#')
		{
			m_lines.emplace_back(s);
			// accept the string
			// std::cout << s << std::endl;
			return true;
		}
		// header was finished reading
		return false;
	}
};

class RecordXMAP: public pg::ITokenizable<HeaderXMAP>
{
public:
/* f int        	int        	int        	float      	float    	float      	float    	string     	float     	string 	float 	float 	int         	string    */
	static constexpr unsigned int m_noItems = 14;
	static constexpr unsigned int m_noRequiredItems = 10;
	static constexpr pg::TokenizableType m_types[m_noItems] = {
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_STRING
	};

// #h XmapEntryID	QryContigID	RefContigID	QryStartPos	QryEndPos	RefStartPos	RefEndPos	Orientation	Confidence	HitEnum	QryLen	RefLen	LabelChannel	Alignment
	/// required fields
	unsigned int 	m_XmapEntryID;
	unsigned int 	m_QryContigID;
	unsigned int 	m_RefContigID;
	float 			m_QryStartPos;
	float 			m_QryEndPos;
	float 			m_RefStartPos;
	float 			m_RefEndPos;
	// std::string 	m_Orientation;
	char		 	m_Orientation;
	float 			m_Confidence;
	std::string 	m_HitEnum;
	// optional fields ??
	float 			m_QryLen;
	float 			m_RefLen;
	unsigned int 	m_LabelChannel;
	std::string 	m_Alignment;

public:
	RecordXMAP()
		: ITokenizable(),
		m_XmapEntryID(0),
		m_QryContigID(0),
		m_RefContigID(0),
		m_QryStartPos(0.f),
		m_QryEndPos(0.f),
		m_RefStartPos(0.f),
		m_RefEndPos(0.f),
		m_Orientation('0'),
		m_Confidence(0.f),
		m_HitEnum(""),
		m_QryLen(0.f),
		m_RefLen(0.f),
		m_LabelChannel(0),
		m_Alignment("")
	{}
	~RecordXMAP() {}

	inline unsigned int getNumberOfItems() override { return m_noItems; }

	inline unsigned int getNumberOfRequiredItems() override { return m_noRequiredItems; }
 
	inline pg::TokenizableType getItemType(const unsigned int id) override
	{
		return (id < RecordXMAP::m_noItems) ? RecordXMAP::m_types[id] : pg::TokenizableType::TT_UNDEFINED;
	}

	inline void setValue(const unsigned int id, const std::string::iterator& start, const std::string::iterator& end) override
	{
		switch (id)
		{
		case 0: toUI(start, end, m_XmapEntryID); return;
		case 1: toUI(start, end, m_QryContigID); return;
		case 2: toUI(start, end, m_RefContigID); return;
		// to make sure there are no negative values for unsigned int
		// case 0: toUI(start, end, m_XmapEntryID, id); return;
		// case 1: toUI(start, end, m_QryContigID, id); return;
		// case 2: toUI(start, end, m_RefContigID, id); return;
		case 3: toF(start, end, m_QryStartPos); return;
		case 4: toF(start, end, m_QryEndPos); return;
		case 5: toF(start, end, m_RefStartPos); return;
		case 6: toF(start, end, m_RefEndPos); return;
		case 7: toC(start, end, m_Orientation); return;
		case 8: toF(start, end, m_Confidence); return;
		case 9: toS(start, end, m_HitEnum); return;
		case 10: toF(start, end, m_QryLen); return;
		case 11: toF(start, end, m_RefLen); return;
		case 12: toUI(start, end, m_LabelChannel); return;
		// case 12: toUI(start, end, m_LabelChannel, id); return;
		case 13: toS(start, end, m_Alignment); return;
		default: return;
		}
	}

	inline void print(void) const
	{
		std::cout
			<< m_XmapEntryID << '\t'
			<< m_QryContigID << '\t'
			<< m_RefContigID << '\t'
			<< m_QryStartPos << '\t'
			<< m_QryEndPos << '\t'
			<< m_RefStartPos << '\t'
			<< m_RefEndPos << '\t'
			<< m_Orientation << '\t'
			<< m_Confidence << '\t'
			<< m_HitEnum << '\t'
			<< m_QryLen << '\t'
			<< m_RefLen << '\t'
			<< m_LabelChannel << '\t'
			<< m_Alignment << '\t'
			<< std::endl;
	}
};

} // namespace bp

#endif // __XMAP_H_
