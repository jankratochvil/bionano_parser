#pragma once
#ifndef __SMAP_H_
#define __SMAP_H_

#include "cTokenizer.h"

// bionanoparser
namespace bp
{

class HeaderSMAP : public pg::IHeader
{
protected:
	std::vector<std::string> m_lines;

public:
	inline bool parseLine(std::string &s) override
	{
		if (s.at(0) == '#')
		{
			m_lines.emplace_back(s);
			// accept the string
			// std::cout << s << std::endl;
			return true;
		}
		// header was finished reading
		return false;
	}
};

class RecordSMAP: public pg::ITokenizable<HeaderSMAP>
{
/* #f int        	int        	int         	int         	float      	float    	float      	float     	float 	string 	int	int	int	int	int	int	int	string	int	int	float	float	float	float	string	string	int	float	string	string	string	float	string */
public:
	static constexpr unsigned int m_noItems = 33;
	static constexpr unsigned int m_noRequiredItems = 24;
	static constexpr pg::TokenizableType m_types[m_noItems] = {
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_FLOAT,
		pg::TokenizableType::TT_STRING
	};

// #h SmapEntryID	QryContigID	RefcontigID1	RefcontigID2	QryStartPos	QryEndPos	RefStartPos	RefEndPos	Confidence	Type	XmapID1	XmapID2	LinkID	QryStartIdx	QryEndIdx	RefStartIdx	RefEndIdx	Zygosity	Genotype	GenotypeGroup	RawConfidence	RawConfidenceLeft	RawConfidenceRight	RawConfidenceCenter	Sample	Algorithm	Size	Present_in_%_of_BNG_control_samples	Fail_assembly_chimeric_score	OverlapGenes	NearestNonOverlapGene	NearestNonOverlapGeneDistance	Found_in_self_molecules
	/// required fields
	unsigned int 	m_SmapEntryID;
	unsigned int 	m_QryContigID;
	int 			m_RefcontigID1;
	int 			m_RefcontigID2;
	float 			m_QryStartPos;
	float 			m_QryEndPos;
	float 			m_RefStartPos;
	float 			m_RefEndPos;
	float 			m_Confidence;
	std::string 	m_Type;
	int 			m_XmapID1;
	int 			m_XmapID2;
	int			 	m_LinkID;
	int 			m_QryStartIdx;
	int 			m_QryEndIdx;
	int 			m_RefStartIdx;
	int 			m_RefEndIdx;
	std::string 	m_Zygosity;
	int 			m_Genotype;
	int 			m_GenotypeGroup;
	float 			m_RawConfidence;
	float 			m_RawConfidenceLeft;
	float 			m_RawConfidenceRight;
	float 			m_RawConfidenceCenter;
	// optional fields ??
	std::string 	m_Sample;
	std::string 	m_Algorithm;
	int 			m_Size;
	float 			m_Present_In_Samples; // Present_in_%_of_BNG_control_samples
	std::string 	m_Fail_Assembly_Chimeric_Score;
	std::string 	m_OverlapGenes;
	std::string 	m_NearestNonOverlapGene;
	float 			m_NearestNonOverlapGeneDistance;
	std::string 	m_Found_In_Self_Molecules;

public:
	RecordSMAP()
		: ITokenizable(),
		m_SmapEntryID(0),
		m_QryContigID(0),
		m_RefcontigID1(0),
		m_RefcontigID2(0),
		m_QryStartPos(0.f),
		m_QryEndPos(0.f),
		m_RefStartPos(0.f),
		m_RefEndPos(0.f),
		m_Confidence(0.f),
		m_Type(""),
		m_XmapID1(0),
		m_XmapID2(0),
		m_LinkID(0),
		m_QryStartIdx(0),
		m_QryEndIdx(0),
		m_RefStartIdx(0),
		m_RefEndIdx(0),
		m_Zygosity(""),
		m_Genotype(0),
		m_GenotypeGroup(0),
		m_RawConfidence(0.f),
		m_RawConfidenceLeft(0.f),
		m_RawConfidenceRight(0.f),
		m_RawConfidenceCenter(0.f),
		m_Sample(""),
		m_Algorithm(""),
		m_Size(0),
		m_Present_In_Samples(0.f), // Present_in_%_of_BNG_control_samples
		m_Fail_Assembly_Chimeric_Score(""),
		m_OverlapGenes(""),
		m_NearestNonOverlapGene(""),
		m_NearestNonOverlapGeneDistance(0.f),
		m_Found_In_Self_Molecules("")
	{}
	~RecordSMAP() {}

	inline unsigned int getNumberOfItems() override { return m_noItems; }

	inline unsigned int getNumberOfRequiredItems() override { return m_noRequiredItems; }
 
	inline pg::TokenizableType getItemType(const unsigned int id) override
	{
		return (id < RecordSMAP::m_noItems) ? RecordSMAP::m_types[id] : pg::TokenizableType::TT_UNDEFINED;
	}

	inline void setValue(const unsigned int id, const std::string::iterator& start, const std::string::iterator& end) override
	{
		switch (id)
		{
		// case 0: toUI(start, end, m_SmapEntryID, id); return;
		// case 1: toUI(start, end, m_QryContigID, id); return;
		case 0: toUI(start, end, m_SmapEntryID); return;
		case 1: toUI(start, end, m_QryContigID); return;
		case 2: toI(start, end, m_RefcontigID1); return;
		case 3: toI(start, end, m_RefcontigID2); return;
		case 4: toF(start, end, m_QryStartPos); return;
		case 5: toF(start, end, m_QryEndPos); return;
		case 6: toF(start, end, m_RefStartPos); return;
		case 7: toF(start, end, m_RefEndPos); return;
		case 8: toF(start, end, m_Confidence); return;
		case 9: toS(start, end, m_Type); return;
		case 10: toI(start, end, m_XmapID1); return;
		case 11: toI(start, end, m_XmapID2); return;
		case 12: toI(start, end, m_LinkID); return;
		case 13: toI(start, end, m_QryStartIdx); return;
		case 14: toI(start, end, m_QryEndIdx); return;
		case 15: toI(start, end, m_RefStartIdx); return;
		case 16: toI(start, end, m_RefEndIdx); return;
		case 17: toS(start, end, m_Zygosity); return;
		case 18: toI(start, end, m_Genotype); return;
		case 19: toI(start, end, m_GenotypeGroup); return;
		case 20: toF(start, end, m_RawConfidence); return;
		case 21: toF(start, end, m_RawConfidenceLeft); return;
		case 22: toF(start, end, m_RawConfidenceRight); return;
		case 23: toF(start, end, m_RawConfidenceCenter); return;
		case 24: toS(start, end, m_Sample); return;
		case 25: toS(start, end, m_Algorithm); return;
		case 26: toI(start, end, m_Size); return;
		case 27: toF(start, end, m_Present_In_Samples); return;
		case 28: toS(start, end, m_Fail_Assembly_Chimeric_Score); return;
		case 29: toS(start, end, m_OverlapGenes); return;
		case 30: toS(start, end, m_NearestNonOverlapGene); return;
		case 31: toF(start, end, m_NearestNonOverlapGeneDistance); return;
		case 32: toS(start, end, m_Found_In_Self_Molecules); return;
		default: return;
		}
	}

	inline void print(void) const
	{
		std::cout
		<< m_SmapEntryID << '\t'
		<< m_QryContigID << '\t'
		<< m_RefcontigID1 << '\t'
		<< m_RefcontigID2 << '\t'
		<< m_QryStartPos << '\t'
		<< m_QryEndPos << '\t'
		<< m_RefStartPos << '\t'
		<< m_RefEndPos << '\t'
		<< m_Confidence << '\t'
		<< m_Type << '\t'
		<< m_XmapID1 << '\t'
		<< m_XmapID2 << '\t'
		<< m_LinkID << '\t'
		<< m_QryStartIdx << '\t'
		<< m_QryEndIdx << '\t'
		<< m_RefStartIdx << '\t'
		<< m_RefEndIdx << '\t'
		<< m_Zygosity << '\t'
		<< m_Genotype << '\t'
		<< m_GenotypeGroup << '\t'
		<< m_RawConfidence << '\t'
		<< m_RawConfidenceLeft << '\t'
		<< m_RawConfidenceRight << '\t'
		<< m_RawConfidenceCenter << '\t'
			// optional fields ??
		<< m_Sample << '\t'
		<< m_Algorithm << '\t'
		<< m_Size << '\t'
		<< m_Present_In_Samples << '\t'
		<< m_Fail_Assembly_Chimeric_Score << '\t'
		<< m_OverlapGenes << '\t'
		<< m_NearestNonOverlapGene << '\t'
		<< m_NearestNonOverlapGeneDistance << '\t'
		<< m_Found_In_Self_Molecules << '\t'
		<< std::endl;
	}
};

} // namespace bp

#endif // __SMAP_H_
