#pragma once
#ifndef __BED_H_
#define __BED_H_

#include "cTokenizer.h"

// bionanoparser
namespace bp
{

class HeaderBED : public pg::IHeader
{
public:
	inline bool parseLine(std::string &s) override
	{
		return false;
	}
// unused at the moment, bionano does not enforce header in BED files
};

class RecordBED: public pg::ITokenizable<HeaderBED>
{
public:
	static constexpr unsigned int m_noItems = 9;
	static constexpr unsigned int m_noRequiredItems = 3;
	static constexpr pg::TokenizableType m_types[m_noItems] = {
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_STRING,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_CHAR,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_INT,
		pg::TokenizableType::TT_RGB
	};

	/// required fields
	std::string 	m_chrom; /// name of the chromosome, scaffold or contig. *SHOULD* be numeric (there should be no "chr" prefix, but bionano files DO include "chr" prefix!
	unsigned int 	m_chromStart; /// starting position
	unsigned int 	m_chromEnd; /// ending position
	/// optional fields
	std::string 	m_name; /// name or type, "gap", "common", "segdupe" 
	unsigned int 	m_score; /// used as id, should be in <0, 1000>, bionano does not enforce this
	char 			m_strand; /// '+' or '-'
	unsigned int 	m_thickStart; /// starting position, expected to be consistent with m_chromStart, currently dummy field
	unsigned int 	m_thickEnd; /// ending position, expected to be consistent with m_chromEnd, currently dummy field
	Rgb				m_itemRgb; // display color in RGB, source should be "R,G,B" comma delimited


public:
	RecordBED()
		: ITokenizable(),
		m_chrom(""),
		m_chromStart(0),
		m_chromEnd(0),
		m_name(""),
		m_score(0),
		m_strand('0'),
		m_thickStart(0),
		m_thickEnd(0),
		m_itemRgb(0,0,0)
	{}
	~RecordBED() {}

	inline unsigned int getNumberOfItems() override { return m_noItems; }

	inline unsigned int getNumberOfRequiredItems() override { return m_noRequiredItems; }
 
	inline pg::TokenizableType getItemType(const unsigned int id) override
	{
		return (id < RecordBED::m_noItems) ? RecordBED::m_types[id] : pg::TokenizableType::TT_UNDEFINED;
	}

	inline void setValue(const unsigned int id, const std::string::iterator& start, const std::string::iterator& end) override
	{
		switch (id)
		{
		case 0: toS(start, end, m_chrom); return;
		case 1: toUI(start, end, m_chromStart); return;
		case 2: toUI(start, end, m_chromEnd); return;
		case 3: toS(start, end, m_name); return;
		case 4: toUI(start, end, m_score); return;
		case 5: toC(start, end, m_strand); return;
		case 6: toUI(start, end, m_thickStart); return;
		case 7: toUI(start, end, m_thickEnd); return;
		case 8: toRGB(start, end, m_itemRgb); return;
		default: return;
		}
	}

	inline void print(void) const
	{
		std::cout
			<< m_chrom << '\t'
			<< m_chromStart << '\t'
			<< m_chromEnd << '\t'
			<< m_name << '\t'
			<< m_score << '\t'
			<< m_strand << '\t'
			<< m_thickStart << '\t'
			<< m_thickEnd << '\t'
			<< m_itemRgb << '\t'
			<< std::endl;
	}
};

} // namespace bp

#endif // __BED_H_
