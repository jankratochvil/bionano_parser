#pragma once
#ifndef __RGB_H_
#define __RGB_H_

#include <ostream>

class Rgb
{
public:
	unsigned char r;
	unsigned char g;
	unsigned char b;
	Rgb(unsigned char _r, unsigned char _g, unsigned char _b)
		: r(_r), g(_g), b(_b) {}

	friend std::ostream &operator<<( std::ostream &output, const Rgb &value ) {
		output << static_cast<int>(value.r) << "," << static_cast<int>(value.g) << "," << static_cast<int>(value.b);
		return output;
	}

};

#endif // __RGB_H_

