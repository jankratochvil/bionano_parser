#!/bin/bash
# make -C ./build/ clean
# files="./build/ ./bin/"
# for i in $files; do
# 	rm -rf "$i"
# done

# used to cleanup trash files generated by attempted in-source build 
echo ""
cleanup="CMakeFiles CMakeCache.txt"
for i in $cleanup; do
	rm -rf  "$i"
done

cmake -P "clean-all.cmake" .
echo ""


